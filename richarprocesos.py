
def registro(opcion, diccionario1):
    "Este proceso fue hecho para que el usuario pueda registrar a una o varias personas, asi como imprimir a las personas registradas"
    nombre = 0
    cedula = 0
    if opcion == "A" or "a":
        people = int(input("Digite la cantidad de personas que desee: "))
        for i in range(people):
            cedula = int(input("Digite el numero de cedula de la persona: "))
            nombre = input("Digite el nombre de la persona: ")
            diccionario1[cedula]=nombre

def verificar(opcion, nombre, diccionario1):
    "Este proceso muestra las personas que estan registradas, en caso de que el usuario desee verificar"
    if opcion == "B" or "b":
        diccionario1.get(nombre)
        print(diccionario1)

def delete(opcion, diccionario1):
    "En este proceso se le permite al usuario la posibilidad de eliminar a cualquier persona registrada, solamente utilizando el numero de cedula"
    if opcion == "C" or "c":
        eliminarCedula = int(input("Digite la cedula de la persona que desea eliminar: "))
        diccionario1.pop(eliminarCedula)
        print(diccionario1)

def consult(opcion, diccionario1):
    "Este proceso sirve para consultar personas que queden registradas si en algun momento el usuario decidio eliminar a alguien"
    if opcion == "D" or "d":
        consultarNombre2 = int(input("Digite la cedula de la persona que desea consultar: "))
        print(diccionario1.get(consultarNombre2))


def modificar(opcion, diccionario1):
    "Este proceso le permite al usuario modificar el nombre de una persona registrada por otro a su conveniencia, solamente ingresando el numero de cedula "
    if opcion == "E" or "e":
        change = int(input("Digite la cedula de la persona que desea cambiar: "))
        name3 = input("Digite el nuevo nombre de la persona: ")
        diccionario1[change]=name3
        print(diccionario1)
